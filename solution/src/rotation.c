#include "../include/rotation.h"


bool rotate_image(struct image* input_img, struct image* output_img){
    output_img->height = input_img->width;
    output_img->width = input_img->height;
    output_img->data = malloc(output_img->height * output_img->width * sizeof(struct pixel));
    if(!output_img->data){
        return false;
    }
    for(size_t y = 0; y < output_img->height; y++){
        for(size_t x = 0; x < output_img->width; x++){
            output_img->data[output_img->width * y + x] = input_img->data[(input_img->height - 1 - x)*input_img->width+y];
        }
    }

    return true;
}


bool get_rotated_image(struct bmp_header* input_header, struct image* input_img, struct bmp_header* output_header, struct image* output_img){
    *output_header = *input_header;
    output_header->biWidth = input_img->height;
    output_header->biHeight = input_img->width;

    return rotate_image(input_img, output_img);
}
