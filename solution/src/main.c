#include "../include/bmp/handler.h"
#include "rotation.h"


void free_headers_and_images(struct bmp_header* input_header, struct image* input_image, struct bmp_header* output_header, struct image* output_image){
    free(input_header);
    free(input_image);
    free(output_header);
    free(output_image);
}


int main( int argc, char** argv ) {
    (void) argc; (void) argv;

    if (argc > 3){
        fprintf(stderr, "There are too many parameters (we expect 3)!\n");
        return 1;
    }
    else if(argc < 3) {
        fprintf(stderr, "There is not enough parameters (we expect 3)!\n");
        return 1;
    }

    const char* input_path = argv[1];
    const char* output_path = argv[2];


    struct bmp_header* input_header = malloc(sizeof(struct bmp_header));
    if(!input_header) {return 1;}
    struct image* input_image = malloc(sizeof(struct image));
    if(!input_image) {free(input_header); return 1;}
    struct bmp_header* output_header = malloc(sizeof(struct bmp_header));
    if(!output_header) {free(input_header); free(input_image); return 1;}
    struct image* output_image = malloc(sizeof(struct image));
    if(!output_image) {free(input_header); free(input_image);free(output_header); return 1;}


    enum read_status our_read_status = read_data_from_file(input_path, input_header, input_image);
    if(our_read_status != READ_OK){
        free_headers_and_images(input_header, input_image, output_header, output_image);
        fprintf(stderr, "%s", get_read_status_comment(our_read_status));
        return 1;
    }

    if(!get_rotated_image(input_header, input_image, output_header, output_image)){
        free(input_image->data);
        free_headers_and_images(input_header, input_image, output_header, output_image);
        fprintf(stderr, "There is not enough memory to make rotated copy of image!\n");
        return 1;
    }

    enum write_status our_write_status = write_data_to_file(output_path, output_header, output_image);
    if(our_write_status != WRITE_OK){
        free(input_image->data);
        free(output_image->data);
        free_headers_and_images(input_header, input_image, output_header, output_image);
        fprintf(stderr, "%s", get_write_status_comment(our_write_status));
        return 1;
    }

    free(input_image->data);
    free(output_image->data);
    free_headers_and_images(input_header, input_image, output_header, output_image);

    return 0;
}
