#include "../../include/bmp/handler.h"


enum read_status read_data_from_file(const char* path_to_file, struct bmp_header* header_from_file, struct image* img_from_file){
    if (!path_to_file){
        return READ_INCORRECT_PATH;
    }

    FILE* input_file = fopen(path_to_file, "rb");
    if(!input_file){
        return READ_CANT_OPEN_FILE;
    }

    if(!read_header(input_file, header_from_file)){
        fclose(input_file);
        return READ_CANT_READ_HEADER;
    }


    img_from_file->height = header_from_file->biHeight;
    img_from_file->width = header_from_file->biWidth;
    img_from_file->data = malloc(img_from_file->width * img_from_file->height * sizeof(struct pixel));
    if(!img_from_file->data){
        fclose(input_file);
        return READ_MEMORY_ERROR;
    }

    if(!read_image(input_file, img_from_file)){
        free(img_from_file->data);
        fclose(input_file);
        return READ_CANT_READ_IMAGE;
    }

    fclose(input_file);
    return READ_OK;
}


enum write_status write_data_to_file(const char* path_to_file, struct bmp_header* header, struct image* img){
    if(!path_to_file){
        return WRITE_INCORRECT_PATH;
    }

    FILE* output_file = fopen(path_to_file, "wb");
    if(!output_file){
        return WRITE_CANT_OPEN_FILE;
    }

    if(!write_header(output_file, header)){
        return WRITE_CANT_WRITE_HEADER;
    }

    if(!write_image(output_file, img)){
        return WRITE_CANT_WRITE_IMAGE;
    }

    return WRITE_OK;
}


const char* get_read_status_comment(enum read_status status){
    const char* read_status_comments[] = {
            "Everything is ok!\n",
            "Can't read your path to the input file!\n",
            "Can't open your input file for reading!\n",
            "Can't read header from your input file!\n",
            "There is not enough memory to upload image!\n",
            "Can't read image from your input file!\n"
    };
    return read_status_comments[status];
}


const char* get_write_status_comment(enum write_status status){
    const char* write_status_comments[] = {
            "Everything is ok!\n",
            "Can't read your path to the output file!\n",
            "Can't open your output file for writing!\n",
            "Can't write header in your output file!\n",
            "Can't write image in your output file!\n"
    };
    return write_status_comments[status];
}





