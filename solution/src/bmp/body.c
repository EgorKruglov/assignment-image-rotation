#include "../../include/bmp/body.h"

#define PIXEL_SIZE  sizeof(struct pixel)

uint64_t calculate_padding(uint64_t width){
    uint64_t bytes_in_row = width * PIXEL_SIZE;
    return (4 - (bytes_in_row % 4))%4;
}


bool read_image(FILE* input_file, struct image* img){
    uint64_t padding = calculate_padding(img->width);
    for(size_t i = 0; i < img->height; i++){
        if(fread(&(img->data[i*img->width]), PIXEL_SIZE, img->width, input_file) != img->width){
            return false;
        }
        fseek(input_file, (long) padding, SEEK_CUR);
    }
    return true;
}


bool write_image(FILE* output_file, struct image* img){
    uint64_t padding = calculate_padding(img->width);

    for (size_t y = 0; y < img->height; y++){
        if(fwrite(&img->data[y*img->width], img->width*PIXEL_SIZE, 1, output_file) != 1){
            return false;
        }

        for(size_t i = 0; i < padding; i++){
            fputc(0, output_file);
        }
    }
    return true;
}
