#include "../../include/bmp/header.h"


bool read_header( FILE* inputFile, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, inputFile ) == 1;
}

bool write_header(FILE* output_file, struct bmp_header* header){
    return fwrite(header, sizeof(struct bmp_header), 1, output_file) == 1;
}


