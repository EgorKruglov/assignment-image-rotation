#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

#include "bmp/body.h"
#include "bmp/header.h"
#include <malloc.h>

bool get_rotated_image(struct bmp_header* input_header, struct image* input_img, struct bmp_header* output_header, struct image* output_img);
#endif
