#ifndef IMAGE_TRANSFORMER_BODY_H
#define IMAGE_TRANSFORMER_BODY_H

#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
};
struct image {
    uint64_t width;
    uint64_t height;
    struct pixel* data;
};

bool read_image(FILE* input_file, struct image* img);
bool write_image(FILE* output_file, struct image* img);
#endif
