#ifndef IMAGE_TRANSFORMER_HANDLER_H
#define IMAGE_TRANSFORMER_HANDLER_H

#include "body.h"
#include "header.h"
#include <malloc.h>

enum read_status{
    READ_OK,
    READ_INCORRECT_PATH,
    READ_CANT_OPEN_FILE,
    READ_CANT_READ_HEADER,
    READ_MEMORY_ERROR,
    READ_CANT_READ_IMAGE
};

enum write_status{
    WRITE_OK,
    WRITE_INCORRECT_PATH,
    WRITE_CANT_OPEN_FILE,
    WRITE_CANT_WRITE_HEADER,
    WRITE_CANT_WRITE_IMAGE
};

enum read_status read_data_from_file(const char* path_to_file, struct bmp_header* header_from_file, struct image* img_from_file);
enum write_status write_data_to_file(const char* path_to_file, struct bmp_header* header, struct image* img);
const char* get_read_status_comment(enum read_status status);
const char* get_write_status_comment(enum write_status status);


#endif
